<?php
/**
 * @file
 * interior-search-form-wrapper.func.php
 */

/**
 * Theme function implementation for interior_search_form_wrapper.
 */
function interior_search_form_wrapper($variables) {
  $output = '<div class="input-group">';
  $output .= $variables['element']['#children'];
  $output .= '<span class="input-group-btn">';
  $output .= '<button type="submit" class="btn btn-default">';
  // We can be sure that the font icons exist in CDN.
  if (theme_get_setting('interior_cdn')) {
    $output .= _interior_icon('search');
  }
  else {
    $output .= t('Search');
  }
  $output .= '</button>';
  $output .= '</span>';
  $output .= '</div>';
  return $output;
}
