<?php
/**
 * @file
 * fieldset.func.php
 */

/**
 * Overrides theme_fieldset().
 */
function interior_fieldset($variables) {
  return theme('interior_panel', $variables);
}
