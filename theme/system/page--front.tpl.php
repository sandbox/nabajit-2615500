 
	
	<!-- header -->
	<header>
	
		<div class="top-nav">
			<div class="container">
				<!-- top social icons -->
				<div class="pull-left">
				<ul class="social">
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
				</ul>
				</div>
				<!-- /.social -->
                                
				 
				 <div class="pull-right">
                                
				
				<!-- top right links -->
				<?php if (!empty($secondary_nav)): ?>
                                    <div class="pull-right"> <?php print render($secondary_nav); ?></div>
				    
                                <?php endif; ?>
				</div>
				<!-- /.social -->
			</div>
			<!-- /.container -->
		</div>
		<!-- /.top-nav -->
		
		<!-- Main Navigation -->
		<nav class="<?php print $navbar_classes; ?> " role="navigation">
			<div class="container">
				<div class="navbar-header">
                              <!-- toogle button for mobile (xs) devices -->
                              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse"> <i class="fa fa-bars fa-3x"></i> </button>
                              
 	
				<!-- logo -->
				<?php if ($logo): ?>
                                <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
                                  <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="img-responsive" />
                                </a>
                               <?php endif; ?>

                                <?php if (!empty($site_name)): ?>
                                <a class="name navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
                                <?php endif; ?>
				</div>
					
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right navbar-main-collapse">
					 <?php if (!empty($primary_nav)): ?>
                                            <?php print render($primary_nav); ?>
                                         <?php endif; ?>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container -->
		</nav>
		<!-- /.main-nav -->
		
	<!-- Banner Slider -->
	<?php    $slider  = theme_get_setting('slider'); ?>
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<?php
				   $count = count($slider);
				   $i=0;
				?>
				<?php for($i=0; $i < count($slider); $i++){?>
                                    <li data-target="#carousel-example-generic" data-slide-to="<?php print $i;?>" <?php print $i == 0 ? 'class="active"':'';?>></li>
				 
                                <?php } ?>
				
				<!--<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-example-generic" data-slide-to="1"></li>
				<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				<li data-target="#carousel-example-generic" data-slide-to="3"></li>!-->
			</ol>
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<?php    foreach($slider as $key=> $value): ?> 
				<div class="item <?php print $key == 0 ?  'active' : '' ?>"> <img src="<?php print $value['slider_image'] ?>" alt="banner1">
					<div class="carousel-caption">
						<h2><i><?php print $value['slider_title'] ?></i></h2>
					</div>
				</div>
				<?php   endforeach; ?>
				 
			</div>
			<!-- Controls -->
			<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> </a>
			<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> </a>
		</div>
		<!-- /.carousel -->
		
	</header>
	<!-- /.header -->

	<!-- Main Contents Container -->
 <div class="front-container">

  <header role="banner" id="page-header">
    <?php if (!empty($site_slogan)): ?>
      <p class="lead"><?php print $site_slogan; ?></p>
    <?php endif; ?>

    <?php print render($page['header']); ?>
  </header> <!-- /#page-header -->

  <div class="row">

    

    <section class="col-sm-12">
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php if (!empty($title)): ?>
        <h1 class="page-header"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      
      <?php if (!empty($page['front_content'])): ?>
        <?php print render($page['front_content']); ?>
	 <?php else: ?>
	  <div class="region region-front-content">
             <h1 class="page-header">Please add something in front page content block  </h1>
          </div>
      <?php endif; ?>
      
      
    </section>

   

  </div>
</div>
	<!--/.main-->

<!-- Footer -->
<footer class="dark-bg">
        <div class="container">
                <div class="row">
                        <!-- Footer Big Area -->
                        <div class="footerBlock">
                                 <?php print render($page['footer']); ?>
                        </div>
                        <!-- /.footerBlock -->
                </div>
                <!-- /.row -->
        </div>
        <!-- .container -->
        
        <!-- Footer Bottom -->
        <div class="footer-bottom">
                <div class="container">
                         <?php print render($page['footer_bottom']); ?>
                </div>
                <!-- .container -->
        </div>
        <!-- .footer-bottom -->
        
</footer>
<!--/.footer-->

 
 