 
	
	<!-- header -->
	<header>
	
		<div class="top-nav">
			<div class="container">
				<!-- top social icons -->
				<ul class="social pull-left">
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
				</ul>
				<!-- /.social -->
				<!-- top right links -->
				<?php if (!empty($secondary_nav)): ?>
                                    <?php print render($secondary_nav); ?>
                                <?php endif; ?>
				<!-- /.social -->
			</div>
			<!-- /.container -->
		</div>
		<!-- /.top-nav -->
		
		<!-- Main Navigation -->
		<nav class="<?php print $navbar_classes; ?>" role="navigation">
			<div class="container">
				<div class="navbar-header">
                              <!-- toogle button for mobile (xs) devices -->
                              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse"> <i class="fa fa-bars fa-3x"></i> </button>
                              
 	
				<!-- logo -->
				<?php if ($logo): ?>
                                <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
                                  <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="img-responsive" />
                                </a>
                               <?php endif; ?>

                                <?php if (!empty($site_name)): ?>
                                <a class="name navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
                                <?php endif; ?>
				</div>
					
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right navbar-main-collapse">
					 <?php if (!empty($primary_nav)): ?>
                                            <?php print render($primary_nav); ?>
                                         <?php endif; ?>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container -->
		</nav>
		<!-- /.main-nav -->
		
		 
		
	</header>
	<!-- /.header -->

<!-- Main Contents Container -->
 <div class="main-container container container-inner">

  <header role="banner" id="page-header">
    <?php if (!empty($site_slogan)): ?>
      <p class="lead"><?php print $site_slogan; ?></p>
    <?php endif; ?>

    <?php print render($page['header']); ?>
  </header> <!-- /#page-header -->

  <div class="row">
 
    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php
     // print '<pre>';
       // print_r($page['sidebar_first']);
         // print '</pre>';
        ?>
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>

    <section<?php print $content_column_class; ?>>
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php if (!empty($title)): ?>
        <h1 class="page-header"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
    </section>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>

  </div>
</div>
	<!--/.main-->

<!-- Footer -->
<footer class="dark-bg">
        <div class="container">
                <div class="row">
                        <!-- Footer Big Area -->
                        <div class="footerBlock">
                                 <?php print render($page['footer']); ?>
                        </div>
                        <!-- /.footerBlock -->
                </div>
                <!-- /.row -->
        </div>
        <!-- .container -->
        
        <!-- Footer Bottom -->
        <div class="footer-bottom">
                <div class="container">
                         <?php print render($page['footer_bottom']); ?>
                </div>
                <!-- .container -->
        </div>
        <!-- .footer-bottom -->
        
</footer>
<!--/.footer-->

 
 