<?php
/**
 * @file
 * menu-tree.func.php
 */

/**
 * Overrides theme_menu_tree().
 */
function interior_menu_tree(&$variables) {
  return '<ul class="menu nav">' . $variables['tree'] . '</ul>';
}

/**
 * interior theme wrapper function for the primary menu links.
 */
function interior_menu_tree__primary(&$variables) {
  return '<ul class="menu nav navbar-nav">' . $variables['tree'] . '</ul>';
}

/**
 * interior theme wrapper function for the secondary menu links.
 */
function interior_menu_tree__secondary(&$variables) {
  $menu = '<ul class="secondary topright">';
  $menu .= '<li class="search-form leaf">' . drupal_render(drupal_get_form('search_form')) .'</li>';
  $menu .= $variables['tree'];
  $menu .= '</ul>';
  
  
  
  return $menu;
}
